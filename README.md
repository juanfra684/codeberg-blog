# Codeberg Blog
This repository is responsible for the publication of the Codeberg blog at https://blog.codeberg.org.

It is using Pelican - you can read more about it at https://blog.getpelican.com/

## Contributing
Like everything Codeberg, the blog is absolutely meant to be open for contributions! Feel free to write an interesting article or make changes to the design of the blog.

To contribute, just fork the repository using the Fork button of Codeberg. You can then clone the repository to your local machine, make, commit and push changes and finally create a Pull Request to this repository (If you are unfamiliar with the general git workflow, check out the Codeberg docs for help: https://codeberg.org/Codeberg/Documentation). As soon as your PR is merged, the blog will get updated with your content.

### Adding content
Content of the blog resides in content/ as Markdown (.md) files. You can add your own files and follow the workflow described above.

### Make changes to the theme
The theme of the blog and content related to it can be found in themes/codeberg/ . You will find a dedicated README.md about technical aspects of the pelican theme.

### Installing Pelican
You require pelican to build the blog. On many Linux distributions, there is already a package called 'pelican'. Try to install it with your local package manager, e. g. on Debian based distros `sudo apt install pelican`. This is the recommended way as it will be easier to handle dependencies and updates.
If that was no success but you already have Python and pip installed, you can also issue
```
pip install pelican[Markdown]
```
in your terminal to install pelican to your machine. To install it globally, you require root access, you might need to add `sudo` to the command above. You can learn more about Pelican installation at https://docs.getpelican.com/en/stable/install.html.

If you still need Python and Pip, refer to the Python and Pip docs on how to install it to your machine.

### Local testing and development
To preview the changes you made (not necessary when writing articles but recommended when you make changes to the pelican setup), you can use the commands in the Makefile. 
To build the pelican site to the path specified in the Makefile at `OUTPUTDIR` (default: /var/www/blog.codeberg.org) run `pelican publish`. If you don't want to set up a web server serving this directory, you can also run `pelican serve` to serve the site locally at http://localhost:8000
In case that directory does not exist on your machine, you can create it or change the OUTPUTDIR to another path.

Title: Monthly Report July 2020
Date: 2020-08-12
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.


We are hosting 4538 repositories, created and maintained by 3588 users. Compared to one month ago, this is an organic growth rate of +540 repositories (+13.5% month-over-month) and +420 users (+13.3%).

The machines have been upgraded and are runnnig at about 43% capacity, as usual we will scale up once again we approach the 66% threshold. In the coming weeks we plan to purchase hardware for an economic mid-end backup server doing automated offsite backups (job currently running on machine provided by founding members). If you would like to help in this project -- configuring/building/setting up the box -- please tell us!

Codeberg e.V. has 75 members in total, these are 58 members with active voting rights and 17 supporting members.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--<br/>

https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929. 


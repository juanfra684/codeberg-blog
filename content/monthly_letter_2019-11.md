Title: Monthly Report October 2019
Date: 2019-11-04
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month)

Dear Codeberg e.V. Members and Supporters!

It is time for updates.

Our annual general assembly will be scheduled for January, exact date will be announced with the invitation that will go out next days. For all non-local members we will set up a dial-in. Please watch your inbox for details.

You can now create static pages at https://pages.codeberg.org. Simply push your HTML, content, styles, images, fonts and other assets to a repo named 'pages', in your user or org account. The content is then immediately rendered and served at the URL https://pages.codeberg.org/<username>. 

Codeberg e.V. has 41 active members. We are hosting 1582 repositories, created and maintained by 3530 users. Compared to one month ago, this is an organic growth of rate of +396 repositories (+33% month-over-month), and +1443 users (+69%).

As mentioned in the last letter, the abnormal surge in user registrations is in part attributable to an increasing number of registrations that do not enter plausible user ids for registration (but seemingly random 12-character-strings instead, from the perspective of a human). We will continue to monitor the phenomenon and of course reserve the right to delete them as soon they have to be considered abusive.

The machines are runnnig at about 55% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggested that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers. As we have now passed this threshold, we can now re-run initial calculations and check options for a logical next step.

Codeberg e.V.

--
https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929. 


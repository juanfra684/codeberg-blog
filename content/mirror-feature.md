Title: Mirror repos: easily created, consuming resources forever
Date: 2020-03-11
Category: Features
Authors: Codeberg e.V.

At launch we considered the Gitea mirror feature a great way to smoothly transition repos for projects to Codeberg.org, also as nice way to quickly test-drive Codeberg's features. Over time it turned out however, that many if not the majority of all mirrors become abandoned, and mirror repositories tend to be heavily skewed towards the "huge" category. (And this makes sense after all — if you want to test-drive our resilience, picking the largest repos on earth will put most stress on the system: and you happily recognized that our infrastructure handled this easily).

So what? Now, after heavy and thourough testing — some users created over 100 (!) mirror repos of the largest known Open-Source repositories (several copies of orphaned Linux kernel source trees, the GitLabCE and Electron source code — these abandoned mirrors are updating automatically and continuously consuming resources and adding traffic without tangible benefit to anyone. A significant number of these repos are even tagged 'private', even less beneficial for the FOSS community.

Mirror repos are easily created and steadily growing, once created, consuming resources forever.

Originally we just wanted to disable the creation of private mirrors, but it turned out to be more complicated than originally thought (because private/public can be toggled at any time).

That is why we have currently disabled the creation of new mirrors on codeberg.org.

All already created mirrors continue to be updated. And new manual mirroring can of course still be done by adding multiple remotes and pushing with `git push --mirror` to Codeberg.org.

In the end (as soon such a feature is implemented in Gitea, contributions welcome!), we would like to allow creation of new automatic cron based mirrors on a case by case basis.

Comments? Please join the discussion in the [Codeberg Community Issues tracker](https://codeberg.org/Codeberg/Community/issues)!

Your Codeberg e.V.


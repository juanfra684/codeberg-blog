Title: Monthly Report March 2021
Date: 2021-04-21
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

We are hosting 10810 repositories, created and maintained by 9457 users. Compared to one month ago, this is an organic growth rate of +1179 repositories (+12.2% month-over-month) and +1181 users (+14.3%).

The machines have been upgraded and are running at about 33% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 110 members in total, these are 83 members with active voting rights and 26 supporting members, and one honorary member.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

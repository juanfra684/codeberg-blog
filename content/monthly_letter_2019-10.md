Title: Monthly Report September 2019
Date: 2019-10-07
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month)

Dear Codeberg e.V. Members and Supporters!

It is time for updates.

We are contemplating the form of our annual member assembly. Our bylaws allow a get-together in person, or a 'virtual' (online-) assembly, synchronuous or asynchronuous. We would like to hear your thoughts about the preferred form, please have your say via email, Mastodon https://mastodon.technology/@codeberg, or Twitter chat. The member assembly will read the annual report, and we would potentially have some time to exchange ideas if meeting in person. Bi-annually, board members are elected (next time in 2020 as the founding board is serving from 2018-2020). Nevertheless, if you could imagine to contribute to Codeberg e.V. as board member and would like to do so, please let us know!

Codeberg e.V. has 38 active members. We are hosting 1186 repositories, created and maintained by 2087 users. Compared to one month ago, this is an organic growth of rate of +148 repositories (+14% month-over-month), and +1041 users (+99.5%).

The irregular explosion in the number of registered users is obviously mildly suspiscious, and indeed we are observing an increasing number of registrations that do not enter plausible user ids for registration (but seemingly random 12-character-strings instead, from the perspective of a human). Even after turning on captchas in the sign-on dialogue we continue to see such, albeit at lower rate. The accounts seem to get never activated nor are repos attached. We will continue to monitor the phenomenon and of course reserve the right to delete them as soon they have to be considered abusive. Right now we are merely carefully monitoring in order to avoid potential collateral damage of overly rigorous actions like unconditionally pruning accounts that look 'suspicious' (whatever that means).

Codeberg e.V.


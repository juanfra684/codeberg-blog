Title: Monthly Report September 2020
Date: 2020-10-14
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)



Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

Codeberg.org has a fresh look. A big Thank You to all involved, in particular scott-joe and mray, who did most of the design work, and those who implemented and pushed the pull requests to make this real: momar, n, lhinderberger, opyale!

Also we launched subdomain pages support on https://codeberg.page. Please have a look and have your say on https://codeberg.org/Codeberg/Community/issues/303!

We are hosting 5507 repositories, created and maintained by 4590 users. Compared to one month ago, this is an organic growth rate of +593 repositories (+12.1% month-over-month) and +577 users (+14.4%).

The machines have been upgraded and are runnnig at about 53% capacity, as usual we will scale up once again we approach the 66% threshold. We have registered the codeberg.page domain for 10 years.

Codeberg e.V. has 85 members in total, these are 65 members with active voting rights and 20 supporting members.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.


--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

